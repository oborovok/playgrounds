import UIKit

/*
let testNumber = 36

let evenOdd = testNumber % 2

var answer = 0

answer += 1
answer += 10
answer *= 10

answer >> 3

print(answer)

let x: Int = 46
let y: Int = 10

// 1
let answer1: Int = (x * 100) + y
// 2
let answer2: Int = (x * 100) + (y * 100)
// 3
let answer3: Int = (x * 100) + (y / 10)

let xx = 8 - (4 * 2 + 6 / 3  * 4)


let voltage = 220.0
let current = 12.0

var power: Double {
    return voltage * current
}

print(power)

let resistance: Double = power / (current * current)

print(resistance)

var randomNumber = arc4random() % 7


for _ in 0...10 {
    print(arc4random() % 7)
}

let diceRoll = Int(arc4random_uniform(UInt32(6)))
*/

// MARK: - chapter 2
/*
let age1 = 42
let age2 = 21

let avg1 =  Double((age1 + age2) / 2)

print(avg1)

let firstName = "Oleksandr"
let lastName = "Borovok"

let full = firstName + " " + lastName
let f2 = "\(firstName) \(lastName)"


let tempDate: (averageTemperature: Double, day: Int, _ : (month: Int, year: Int))

let coordinates = (2, 3)
let nameCoordinates = ("row", "column")

//let character1: Character = "Dog"
let character2: Character = "🐶"
let string1: String = "Dog"
let string2: String = "🐶"

//let tuple = (day: 15, month: 8, year: 2015)
//let day = tuple.Day

let number = 10
let multiplier = 5
let summary = "\(number) multiplied by \(multiplier) equals \(number * multiplier)"
*/

let count = 10
var sum = 0

for i in 1...count {
    sum += i
}

print(sum)
