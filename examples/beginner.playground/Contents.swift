//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

public extension NSObject{
    public class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: .controlCharacters).last!
    }
    
    public var nameOfClass: String{
        return NSStringFromClass(type(of: self)).components(separatedBy: .controlCharacters).last!
    }
}

let obj = NSObject()
obj.nameOfClass

NSObject.nameOfClass


class newObjectA: NSObject {
    
}

let objA = newObjectA()
objA.nameOfClass
String(objA.nameOfClass)

String(describing: type(of: objA))
String(describing: objA)

let a = 1.34
let b = 2.56
//let c = b % a
let c = b.truncatingRemainder(dividingBy: a)

let d = c * a
let e = c * b

// x == y * q + r
let x = 8.625
print(x / 0.75)

let q = (x / 0.75).rounded(.towardZero)
// q == 11.0
let r = x.truncatingRemainder(dividingBy: 0.75)
// r == 0.375

let x1 = 0.75 * q + r
// x1 == 8.625

var myArray = [1,13,95,2,6,134,75,29,49]
var sortedArray = myArray.sorted(by: {$0 < $1})


let commonList = ["YYZ", "Toronto Pearson", "DUB", "Dublin"]
let filter = "du"


let filteredArray = commonList.filter{ $0.range(of: filter, options: [.diacriticInsensitive, .caseInsensitive]) != nil }

print(filteredArray)


//let filteredArray = commonList.filter({
//    $0.range(of: filter, options: [.diacriticInsensitive, .caseInsensitive], range: <#T##Range<String.Index>?#>, locale: <#T##Locale?#>)
//})

let dataSource = [
    "Domain CheckService",
    "IMEI check",
    "Compliant about service provider",
    "Compliant about TRA",
    "Enquires",
    "Suggestion",
    "SMS Spam",
    "Poor Coverage",
    "Help Salim"
]
let searchString = "Enq"
let predicate = NSPredicate(format: "SELF contains %@", searchString)
let searchDataSource = dataSource.filter { predicate.evaluate(with: $0) }

print(searchDataSource)


struct Student {
    var firstName: String
    var lastName: String
    var averageMark: Float
}

var student1 = Student(firstName: "Ivan", lastName: "Trubka", averageMark: 8.4)
var student2 = Student(firstName: "Marina", lastName: "Trubka", averageMark: 9.4)
var student3 = Student(firstName: "Alex", lastName: "Lozshko", averageMark: 8.8)

var arrayStudents = [student1, student2, student3]

func getStudentData(array: [Student]) {
    var i = 0
    for a in array {
        i += 1
        print("\(i) \(a)")
    }
}


func sortStudents (array: [Student]) -> [Student]
{
    let sort = array.sorted {(s1, s2) in return (s2.lastName) < (s1.lastName)}
    return sort
    
}
let res = sortStudents(array: arrayStudents)
var anotherArray = arrayStudents


getStudentData(array: res)

//anotherArray[student1.firstName]

anotherArray[0]

anotherArray.first

let student = anotherArray.first
student?.firstName = "Valeri"

print(anotherArray)
