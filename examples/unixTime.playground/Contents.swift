//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let time = 1519663287000

let date1 = Date.init(timeIntervalSince1970: TimeInterval(time))

let stringDate = "26-02-2018 16:41:27.343"

let dateFormatter = DateFormatter()
dateFormatter.dateFormat = "dd-mm-yyyy HH:mm:ss.SSS" //Your date format
//dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
let date = dateFormatter.date(from: stringDate) //according to date format your date string
print(date ?? 0) //Convert String to Date

let d = date?.timeIntervalSince1970

var dict = [1: "abc", 2: "cde"] // dict is of type Dictionary<Int, String>
dict[3] = "efg"
dict.updateValue("dsjhgfkdjhsfdklghsdjhglks", forKey: 4)
print(dict)

