//: Playground - noun: a place where people can play

import UIKit

var d1 = ["a": "b"]
var d2 = ["c": "e"]

extension Dictionary {
    mutating func merge(_ dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

d1.merge(d2)

// MARK: -
struct Company {
    var name: String
    var symbol: String
    
    init(_ symbol: String, name: String) {
        self.name = name
        self.symbol = symbol
    }
}

let array1 = [["AZ.TO" : true]]
let array2 = [["AZ.TO" : "AZ Mining"], ["ABN.V" : "Aben Resources"] , ["AEM" : "Agnico Eagle Mines Ltd"], ["AEM.TO" : "Agnico-Eagle Mines Ltd"]]
//let array3 = array2.reduce([:]) { dic1, dic2 in
////    return dic1.merge(dic2)
//    return
//}

let bookAmount = ["harrypotter":100.0, "junglebook":1000.0]
let reducedBookNamesOnDict = bookAmount.reduce("Books are ") { $0 + $1.key + " " } //or $0 + $1.0 + “”
print(reducedBookNamesOnDict)


let numbers = [2, 3, 4, 5]
let numberSum = numbers.reduce(0) { x, y in
    return x + y
}

print(numberSum)
