//: Playground - noun: a place where people can play
import Foundation
import UIKit

var str = "Hello, playground"

var array = [1, 2, 3, 4]

let a = array

print(a)

array.append(5)
array.append(6)

for item in a {
    print(item)
}

print(a)
print(array)


enum ErrorsToThrow: Error {
    case fileNotFound
    case fileNotReadable
    case fileSizeIsTooHigh
}

func readFiles(path:String) throws ->String {
    if fileNotFound {
        throw ErrorsToThrow.fileNotFound
    } else if fileNotReadable {
        throw ErrorsToThrow.fileNotReadable
    } else if fileSizeIsTooHigh {
        throw ErrorsToThrow.fileSizeIsTooHigh
    }
    
    return “Data from file”
}
