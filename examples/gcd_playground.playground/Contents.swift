//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let main = DispatchQueue.main
let background = DispatchQueue.global()

func taskA() {
    print("A")
    
    main.sync {
        print("C")
        
        background.sync {
            print("E")
        }
        
        print("D")
    }
    
    print("B")
}

func taskB() {
    print("A")
    
    main.async {
        print("C")
        
        background.sync {
            print("E")
        }
        
        print("D")
    }
    
    print("B")
}

func taskC() {
    print("A")
    
    background.sync {
        print("C")
        
        main.sync {
            print("E")
        }
        
        print("D")
    }
    
    print("B")
}

func taskD() {
    print("A")
    
    background.async {
        print("C")
        
        main.async {
            print("E")
        }
        
        print("D")
    }
    
    print("B")
}

func taskE() {
    print("A")
    
    background.sync {
        print("C")
        
        background.sync {
            print("E")
        }
        
        print("D")
    }
    
    print("B")
}

func taskF() {
    print("A")
    
    background.async {
        print("C")
        
        background.async {
            print("E")
        }
        
        print("D")
    }
    
    print("B")
}

print("<---- \(str) ----->")
//print("Task A ")
//taskA()

//print("Task B")
//taskB()

print("Task C")
taskC()

//print("Task D")
//taskD()
//
//print("Task E")
//taskE()
//
//print("Task F")
//taskF()
