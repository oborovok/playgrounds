//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

enum NotificationType: String {
    case like = "LIKE"
    case follow = "FOLLOW"
}

enum NotificationFollowStatus: String {
    case accepted = "ACCEPTED"
    case pending = "PENDING"
}

print(NotificationType.follow.rawValue)
print(NotificationType.like.rawValue)

protocol NotificationContentModel {
    
}

struct NotificationLike: NotificationContentModel {
    var postId: Int
    var postName: String
    
    init() {
        postId = 100
        postName = "Post about something"
    }
    
}

struct NotificationFollow: NotificationContentModel {
    var status: NotificationFollowStatus
    
    init() {
        status = .pending
    }
}

var notification: NotificationContentModel
var type = NotificationType.follow

var like = NotificationLike()
var follow = NotificationFollow()

if type == .like {
    print(like)
} else {
    print(follow)
}
