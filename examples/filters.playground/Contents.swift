//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

struct model {
    let name: String
    let age: Int
    
    init(_ name: String, _ age: Int) {
        self.name = name
        self.age = age
    }
}

let array = [model("Anna", 23), model("Ira", 22), model("Olga", 21)]
let ira = array.filter{$0.name == "Ira"}
//print(ira)

// MARK: -
var indexes = [["DJI": true], ["IXIC": false], ["SPX": false], ["USDX": true], ["NYA": false], ["GSPTSE": false], ["N225": false]]

func filter(_ array: [[String : Bool]], _ boolValue: Bool) -> [[String : Bool]] {
    var dictionaries = [[String : Bool]]()
    
    for dics in array {
//        print(dics)
        
        let dic = dics.filter({$1 == boolValue})
        
        if dic != [:] {
//            print(dic)
            dictionaries.append(dic)
        }
    }
    
    return dictionaries
}

func getKeys(from array: [[String : Bool]]) -> [String] {
    var keys = [String]()
    
    for element in array {
        for k in element.keys {
//            print(k)
            keys.append(k)
        }
    }
    
    return keys
}

let filtered = filter(indexes, true)
let sth = getKeys(from: filtered)
//print(sth)

// MARK: -
func change(value: Bool, key: String) {
    var res = indexes.filter { $0[key] != nil }
    print(res)
    
    // first attempt
//    if let ind = indexes.index(where: { (dic) -> Bool in
//        return dic == res[0]
//    }) {
//        print(ind)
//        res[0][key] = value
//        indexes[ind] = res[0]
//    }
    
    // second
    if let ind  = indexes.index(where: {$0 == res[0]}) {
        print(ind)
        res[0][key] = value
        indexes[ind] = res[0]
    }
}

change(value: false, key: "DJI")

print(indexes)

// MARK: -
let bookAmount = ["harrypotter":100.0, "junglebook":1000.0]
//print(bookAmount.keys)
let filteredArrayOnDict = bookAmount.filter { $1 > 100}

// MARK: - ===============================
var stocksInitial = [["DJI": true], ["IXIC": true], ["USDX": true]]

struct Stock: Hashable {
    var name: String
    var longName: String
    var isOn: Bool = false
    
    init(name: String, longName: String) {
        self.name = name
        self.longName = longName
    }
}

var namesInitial = [Stock(name:"DJI", longName: "DJdfgsI"), Stock(name:"IXIC", longName: "IXIfasdfC"), Stock(name: "SPX", longName: "SPXasdfasdf"), Stock(name: "USDX", longName: "USDXadsfasdfas"), Stock(name: "NYA", longName: "NYAsdfasdfasdf"), Stock(name: "GSPTSE", longName: "GSPTSEdfasdfasdfas"), Stock(name: "N225", longName: "dasfasdfasdgfasgdasdgadsgds")]

func merge(_ stocks: [[String : Bool]], _ names: [Stock]) -> [Stock] {
    var newNames = names
    for items in stocks {
        for item in items {
            if let ind = names.index(where: {$0.name == item.key}) {
                newNames[ind].isOn = item.value
            }
        }
    }
    
    return newNames
}

print(merge(stocksInitial, namesInitial))

let result0 = namesInitial.map { (stock) -> String in
    let s: String = stock.name
    return s
}

let result1 = namesInitial.map { $0.name }
let result2 = namesInitial.map { $0.isOn }

print(result1, result2)

//extension Array where Element: Hashable {
//
//    func after(item: Element) -> Element? {
//        if let index = self.index(of: item), index + 1 < self.count {
//            return self[index + 1]
//        }
//        return nil
//    }
//}

let item = namesInitial[0]
let prevItem = namesInitial.index(where: { $0 == item})
print(prevItem)

let preItemIndex = namesInitial.index(before: 0)
print(prevItem)
let nextItemIndex = namesInitial.index(after: 0)



