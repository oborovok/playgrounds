//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let main = DispatchQueue.main
let background = DispatchQueue.global()
let helper = DispatchQueue(label: "construction_worker_3")

func doSyncWork() {
    background.sync {
        for _ in 1...3 {
            print("Light")
        }
    }
    
    for _ in 1...3 {
        print("Heavy")
    }
}

func doAsyncWork() {
    background.async {
        for _ in 1...3 {
            print("Light")
        }
    }
    
    for _ in 1...3 {
        print("Heavy")
    }
}

print("<---- \(str) ----->")
//print("-------------------- ")
//doSyncWork()

//print("-------------------- ")
//doAsyncWork()

let asianWorker = DispatchQueue(label: "construction_worker_1")
let brownWorker = DispatchQueue(label: "construction_worker_3")

func doLightWork() {
    asianWorker.async {
        for _ in 1...10 {
            print("👷🏼")
        }
    }
    
    brownWorker.async {
        for _ in 1...10 {
            print("👷🏾‍♀️ b")
        }
    }
}

//print("-------------------- ")
//doLightWork()

