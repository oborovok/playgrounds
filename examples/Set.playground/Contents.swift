//: Playground - noun: a place where people can play

import UIKit

enum StudentSex {
    case female, male
}

var str = "Hello, playground"

class Student: Hashable {
    var studentID: Int
    var name: String
    var age: Int
    var cource: Int
    var sex: StudentSex
    
    var hashValue: Int {
        return studentID
    }
    
    init(_ studentID: Int, _ name: String, _ age: Int, _ sex: StudentSex, _ cource: Int) {
        self.studentID = studentID
        self.name = name
        self.age = age
        self.sex = sex
        self.cource = cource
    }
    
    static func ==(lhs: Student, rhs: Student) -> Bool {
        return lhs.studentID == rhs.studentID
    }
}

let ivan = Student.init(1234134, "Ivan", 22, .male, 4)
let vasya = Student.init(2345231432, "Vasya", 21, .male, 3)
let tanya = Student.init(84567465, "Tanya", 18, .female, 1)

var dataSource = [ivan, vasya, tanya]
var nssetSource = Set.init([ivan, vasya, tanya])

print(String(describing:dataSource))
print(String(describing: nssetSource))

nssetSource.insert(ivan)
nssetSource.insert(Student.init(84567465, "Tanya", 18, .female, 1))
nssetSource.count

dataSource.append(ivan)
dataSource.count

let match = "v"

let resultsArray = dataSource.filter { (student) -> Bool in
    student.name.contains(match)
}

let resultSet = nssetSource.filter { (student) -> Bool in
    student.name.contains(match)
}

let resultSet2 = nssetSource.filter { (student) -> Bool in
    student.sex == .female
}

let resultArray2 = dataSource.filter { (student) -> Bool in
    student.sex == .female
}

