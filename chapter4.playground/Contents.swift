import UIKit

var str = "Hello, playground"

let count = 10
var sum = 0

for i in 1...count {
    sum += i
}

print(sum)

let range = 1...10

for i in range {
    print(i * i)
}

for i in range {
    sqrt(Double(i))
}

sum = 0

for row in 0..<8 {
    if row % 2 == 1 {
        print(row)
        for column in 0..<8 {
            sum += row * column
        }
    }
}

print(sum)
